from sense_hat import SenseHat
import time
from random import randint

sense = SenseHat()

E = [0, 0, 0]  # Empty
A = [0, 32, 150]  # Light L Blue
B = [0, 32, 96]  # Blue

sense.clear()

eye = {}

eye["middle"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, E, A, A, E, E, B,
B, E, A, B, B, A, E, B,
B, E, A, B, B, A, E, B,
B, E, E, A, A, E, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["left"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, A, A, E, E, E, B,
B, A, B, B, A, E, E, B,
B, A, B, B, A, E, E, B,
B, E, A, A, E, E, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["right"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, E, E, A, A, E, B,
B, E, E, A, B, B, A, B,
B, E, E, A, B, B, A, B,
B, E, E, E, A, A, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["up"] = [
E, E, B, B, B, B, E, E,
E, B, E, A, A, E, B, E,
B, E, A, B, B, A, E, B,
B, E, A, B, B, A, E, B,
B, E, E, A, A, E, E, B,
B, E, E, E, E, E, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["up_left"] = [
E, E, B, B, B, B, E, E,
E, B, A, A, E, E, B, E,
B, A, B, B, A, E, E, B,
B, A, B, B, A, E, E, B,
B, E, A, A, E, E, E, B,
B, E, E, E, E, E, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["up_right"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, A, A, B, E,
B, E, E, A, B, B, A, B,
B, E, E, A, B, B, A, B,
B, E, E, E, A, A, E, B,
B, E, E, E, E, E, E, B,
E, B, E, E, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["down"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, E, E, E, E, E, B,
B, E, E, A, A, E, E, B,
B, E, A, B, B, A, E, B,
B, E, A, B, B, A, E, B,
E, B, E, A, A, E, B, E,
E, E, B, B, B, B, E, E
]

eye["down_left"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, E, E, E, E, E, B,
B, E, A, A, E, E, E, B,
B, A, B, B, A, E, E, B,
B, A, B, B, A, E, E, B,
E, B, A, A, E, E, B, E,
E, E, B, B, B, B, E, E
]

eye["down_right"] = [
E, E, B, B, B, B, E, E,
E, B, E, E, E, E, B, E,
B, E, E, E, E, E, E, B,
B, E, E, E, A, A, E, B,
B, E, E, A, B, B, A, B,
B, E, E, A, B, B, A, B,
E, B, E, E, A, A, B, E,
E, E, B, B, B, B, E, E
]


def blink():
    eye_blink = {
        1: [
            E, E, B, B, B, B, E, E,
            E, B, E, E, E, E, B, E,
            B, E, E, A, A, E, E, B,
            B, E, A, B, B, A, E, B,
            B, E, A, B, B, A, E, B,
            B, E, E, A, A, E, E, B,
            E, B, E, E, E, E, B, E,
            E, E, B, B, B, B, E, E
        ],
        2: [
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, B, B, B, B, E, E,
            E, B, A, B, B, A, B, E,
            B, E, A, B, B, A, E, B,
            B, E, E, A, A, E, E, B,
            E, B, E, E, E, E, B, E,
            E, E, B, B, B, B, E, E
        ],
        3: [
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, B, B, B, B, E, E,
            B, B, E, A, A, E, B, B,
            E, B, B, B, B, B, B, E,
            E, E, E, E, E, E, E, E
        ],
        4: [
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            E, E, E, E, E, E, E, E,
            B, B, B, B, B, B, B, B,
            E, B, B, B, B, B, B, E,
            E, E, E, E, E, E, E, E
        ]
    }

    moves = list(eye_blink.keys())
    for move in moves:
        sense.set_pixels(eye_blink[move])
        time.sleep(0.2)

    eye_blink[1]
    sense.clear()


eye_moves = list(eye.keys())
sense.set_rotation(270)

while True:
    sense.set_pixels(eye["middle"])
    time.sleep(1)
    k = randint(0, len(eye_moves)-1)
    sense.set_pixels(eye[eye_moves[k]])
    time.sleep(1)
    if k % 2 == 0:
        blink()  # Even
    else:
        pass  # Odd


sense.clear()

