from sense_hat import SenseHat
import time

sense = SenseHat()

# p = (204, 0, 204) # Pink
# g = (0, 102, 102) # Dark Green
# w = (200, 200, 200) # White
# y = (204, 204, 0) # Yellow
# e = (0, 0, 0) # Empty

X = [255, 0, 0] # Red  # Ojo tupla o array???
B = [47, 117, 181] # Blue
L = [155, 194, 230] # Light Blue
# O = [255, 255, 255] # White => Esto enciende el led, no queda bien aqui
O = [0, 0, 0] # Empty = Apagado

sense.clear()
# sense.clear(0, 0, 0) # ¿?

eye = {} 

eye["middle"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, L, L, O, O, B,
B, O, L, O, B, L, O, B,
B, O, L, B, B, L, O, B,
B, O, O, L, L, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["left"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, L, L, O, O, O, B,
B, L, O, B, L, O, O, B,
B, L, B, B, L, O, O, B,
B, O, L, L, O, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["right"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, O, L, L, O, B,
B, O, O, L, O, B, L, B,
B, O, O, L, B, B, L, B,
B, O, O, O, L, L, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["up"] = [
O, O, B, B, B, B, O, O,
O, B, O, L, L, O, B, O,
B, O, L, O, B, L, O, B,
B, O, L, B, B, L, O, B,
B, O, O, L, L, O, O, B,
B, O, O, O, O, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["up_left"] = [
O, O, B, B, B, B, O, O,
O, B, L, L, O, O, B, O,
B, L, O, B, L, O, O, B,
B, L, B, B, L, O, O, B,
B, O, L, L, O, O, O, B,
B, O, O, O, O, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["up_right"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, L, L, B, O,
B, O, O, L, O, B, L, B,
B, O, O, L, B, B, L, B,
B, O, O, O, L, L, O, B,
B, O, O, O, O, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["down"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, O, O, O, O, B,
B, O, O, L, L, O, O, B,
B, O, L, O, B, L, O, B,
B, O, L, B, B, L, O, B,
O, B, O, L, L, O, B, O,
O, O, B, B, B, B, O, O
]

eye["down_left"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, O, O, O, O, B,
B, O, L, L, O, O, O, B,
B, L, O, B, L, O, O, B,
B, L, B, B, L, O, O, B,
O, B, L, L, O, O, B, O,
O, O, B, B, B, B, O, O
]

eye["down_right"] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, O, O, O, O, B,
B, O, O, O, L, L, O, B,
B, O, O, L, O, B, L, B,
B, O, O, L, B, B, L, B,
O, B, O, O, L, L, B, O,
O, O, B, B, B, B, O, O
]

blink = {}

blink[0] = [
O, O, B, B, B, B, O, O,
O, B, O, O, O, O, B, O,
B, O, O, L, L, O, O, B,
B, O, L, O, B, L, O, B,
B, O, L, B, B, L, O, B,
B, O, O, L, L, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

blink[1] = [
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, B, B, B, B, O, O,
O, B, L, O, B, L, B, O,
B, O, L, B, B, L, O, B,
B, O, O, L, L, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

blink[2] = [
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, B, B, B, B, O, O,
B, B, O, L, L, O, B, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]

blink[3] = [
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
B, B, O, O, O, O, B, B,
O, B, B, B, B, B, B, O,
O, O, B, B, B, B, O, O
]

blink[4] = [
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, B, B, B, B, O, O,
B, B, O, L, L, O, B, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]   

blink[5] = [
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, B, B, B, B, O, O,
O, B, L, O, B, L, B, O,
B, O, L, B, B, L, O, B,
B, O, O, L, L, O, O, B,
O, B, O, O, O, O, B, O,
O, O, B, B, B, B, O, O
]   


count = 0

while True: 
    sense.set_pixels(blink[count % len(blink.keys())])
    time.sleep(0.1)
    count += 1

# sense.clear()