from Motor import *
from Led import *
import threading

# Ruedas: 
# Derecha Delantera, Derecha Trasera, 
# Izquierda Delantera, Izquierda Trasera
PWM = Motor()
led=Led()

# Con valor de 1000 (en dos ruedas) no tiene potencia en el suelo,
# Se usa para realizar tests con el robot levantado.

def stop():
    PWM.setMotorModel(0,0,0,0)
    led.colorWipe(led.strip, Color(0,0,0),10)

#Right
def twist_right():
    x = threading.Thread(target=right_blink, args=(3,))
    x.start()
    PWM.setMotorModel(-1500,-1500,2000,2000)
    time.sleep(0.8)
    stop()

#Left
def twist_left():
    x = threading.Thread(target=left_blink, args=(3,))
    x.start()
    PWM.setMotorModel(2000,2000,-1500,-1500) 
    # PWM.setMotorModel(-500,-500,2000,2000) # Peor
    # PWM.setMotorModel(0,-2000,2000,0) # Mal
    time.sleep(0.8)
    stop()

#Forward
def turn_forward():
    PWM.setMotorModel(2000,2000,2000,2000)
    time.sleep(0.5) # 30 cms
    stop()

# Back    
def turn_back():
    PWM.setMotorModel(-2000,-2000,-2000,-2000)
    time.sleep(1)
    stop()

def low_back():
    PWM.setMotorModel(-700,-700,-700,-700)
    time.sleep(0.5)
    stop()

def low_forward():
    PWM.setMotorModel(700,700,700,700)
    time.sleep(0.5) # 9 cms
    # time.sleep(1) # 25 cms
    stop()
    
led0=0 # "Led0"
led1=1 # "Led1"
led2=2 # "Led2"
led3=3 # "Led3"
led4=4 # "Led4"
led5=5 # "Led5"
led6=6 # "Led6"
led7=7 # "Led7"

def right_blink(n):
    for i in range(n):
        led.ledIndexCPG(led0, 125, 125, 0)
        led.ledIndexCPG(led1, 125, 125, 0)
        led.ledIndexCPG(led6, 125, 125, 0)
        led.ledIndexCPG(led7, 125, 125, 0)
        time.sleep(0.25)
        led.ledIndexCPG(led0, 0, 0, 0)
        led.ledIndexCPG(led1, 0, 0, 0)
        led.ledIndexCPG(led6, 0, 0, 0)
        led.ledIndexCPG(led7, 0, 0, 0)
        time.sleep(0.25)
        
def left_blink(n):
    for i in range(n):
        led.ledIndexCPG(led2, 125, 125, 0)
        led.ledIndexCPG(led3, 125, 125, 0)
        led.ledIndexCPG(led4, 125, 125, 0)
        led.ledIndexCPG(led5, 125, 125, 0)
        time.sleep(0.25)
        led.ledIndexCPG(led2, 0, 0, 0)
        led.ledIndexCPG(led3, 0, 0, 0)
        led.ledIndexCPG(led4, 0, 0, 0)
        led.ledIndexCPG(led5, 0, 0, 0)
        time.sleep(0.25)


#twist_right()
# twist_left()
# turn_back()
turn_forward()
# low_forward()